<?php

/**
 * Autorisation d’accéder à l’api d’un autocomplete
 *
 * Nécessite l’accès à écrire.
 * Peut être surchargé par sélecteur
 *
 * - autoriser_auteur_autocomplete_dist()
 * - autoriser_generique_autocomplete_dist()
 *
 * Note: `$opt['args']` reçoit les paramètres d’appel de l’api
 *
 * @param string $faire Action demandée
 * @param string $type Type d'objet ou élément
 * @param int|string|null $id Identifiant
 * @param array $qui Description de l'auteur demandant l'autorisation
 * @param array $opt Options de cette autorisation
 * @return bool true s'il a le droit, false sinon
 **/
function autoriser_autocomplete_dist(string $faire, string $type, $id, array $qui, array $opt): bool {
	return autoriser('ecrire');
}
