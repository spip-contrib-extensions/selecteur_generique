<?php

if (!defined("_ECRIRE_INC_VERSION")) return;


/**
 * Critere `{contenu_auteur_select}` qui recherche un·e auteur / autrice
 *
 * Récupère la valeur de query-string `q` pour effectuer la recherche.
 *
 * @syntaxe
 * - `{contenu_auteur_select} : dans tout le nom
 * - `{contenu_auteur_select strict} : uniquement depuis le début du nom
 *
 * @critere contenu_auteur_select
 */
function critere_contenu_auteur_select_dist($idb, &$boucles, $crit) {
	trigger_deprecation('selecteur_generique', '2.0', 'Using "%s" criteria is deprecated, use "%s" criteria instead.', 'contenu_auteur_select', 'selecteurgenerique_recherche_auteur');
	$boucle = &$boucles[$idb];

	$debut = ($crit->param[0][0]->texte ?? null) === 'strict' ? '' : '%';

	// un peu trop rapide, ca... le compilateur exige mieux (??)
	// on ne cherche pas dans la bio etc
	// si on peut trouver direct dans le nom ou l'email
	$boucle->hash .= "
	// RECHERCHE
	\$s = 1;
	if (\$r = _request('q')) {
		\$r = _q('{$debut}' . \$r . '%');
		\$s = \"(
			auteurs.nom LIKE \$r
			OR auteurs.email LIKE \$r";
	if ($debut) {
		$boucle->hash .= '
			OR auteurs.bio LIKE $r
			OR auteurs.nom_site LIKE $r
			OR auteurs.url_site LIKE $r
		';
	}
	$boucle->hash .= ")\";\n";
	$boucle->hash .= "}";

	$boucle->where[] = '$s';
}

/**
 * Critere `{selecteurgenerique_recherche_auteur}` qui recherche un·e auteur / autrice
 *
 * @syntaxe
 * - `{selecteurgenerique_recherche_auteur #ENV{q}} : dans tout le nom
 * - `{selecteurgenerique_recherche_auteur #ENV{q}, strict} : uniquement depuis le début du nom
 * - `{selecteurgenerique_recherche_auteur #ENV{q}, id} : uniquement sur l'identifiant
 *
 * @critere selecteurgenerique_recherche_auteur
 */
function critere_selecteurgenerique_recherche_auteur_dist($idb, &$boucles, $crit) {
	$boucle = &$boucles[$idb];

	$q = isset($crit->param[0][0])
		? calculer_liste([$crit->param[0][0]], $idb, $boucles, $boucle->id_parent)
		: "''";

	$mode = isset($crit->param[1][0])
		? calculer_liste([$crit->param[1][0]], $idb, $boucles, $boucle->id_parent)
		: "''";

	// un peu trop rapide, ca... le compilateur exige mieux (??)
	// on ne cherche pas dans la bio etc
	// si on peut trouver direct dans le nom ou l'email
	$boucle->hash .= <<<PHP
	// RECHERCHE
	\$s = 1;
	if (\$r = $q) {
		if ($mode === 'id') {
			\$r = sql_quote(\$r);
			\$s = "(auteurs.id_auteur = \$r)";
		} else {
			\$r .= '%';
			if ($mode !== 'strict') {
				\$r = '%' . \$r;
			}
			\$r = sql_quote(\$r);
			\$s = "(
				auteurs.nom LIKE \$r
				OR auteurs.email LIKE \$r";
			if ($mode !== 'strict') {
				\$s .= "
				OR auteurs.bio LIKE \$r
				OR auteurs.nom_site LIKE \$r
				OR auteurs.url_site LIKE \$r";
			}
			\$s .= ")";
		}
	}
PHP;

#dump($boucle->hash);

	$boucle->where[] = '$s';
}
