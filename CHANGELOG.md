# Changelog

## 2.0.1 - 2024-04-30

### Added

- #5 Permettre de rechercher un auteur avec son identifiant

### Fixed

- #4 Correction JS lorsqu’utilisé dans l’espace public

## 2.0.0 - 2023-05-22

### Added

- Critère `{selecteurgenerique_recherche_auteur #ENV{q}}`
- Nécessite le plugin Select2
- Class JS SelecteurGenerique,
- Méthode JS `SelecteurGenerique.formulaire_editer_liens_autocomplete` pour ajouter assez facilement un autocomplete sur un formulaire de liens standard

### Changed

- Nécessite SPIP 4.2 et PHP 7.4+
- Le Javascript utilise Select2, et non plus sur jQuery-ui
- Format du retour ajax des autocompletion (formalisme Select2)

### Fixed

- Autocomplete sur les formulaires d’ajout d’auteurs et de mots

### Security

- Ajout d’une autorisation pour lire l’api d’autocomplétion (accès à ecrire par défaut.)

### Deprecated

- Critère `{contenu_auteur_select}`. Utiliser `{selecteurgenerique_recherche_auteur #ENV{q}}`
- Format `jQuery-ui autocomplete` des retours ajax d’autocompletion

### Removed

- JS pour jQuery autocomplete
- JS d’autocomplete sur le sélecteur de rubrique (inutile, l’UI de SPIP 4.2 est déjà top).
- fichier `selecteur_generique.html` : utiliser `selecteur.api` comme point d’entrée exclusivement.
- Ancienne API (non Json) : retournait un texte, 1 entrée par ligne, de type `label|value` ou `label|label_court|value`. Utiliser maintenant l’API json (select2).
- squelettes `selecteurs/charger_selecteur.html` (ancienne API non JSON)
- fichier `exec/selecteur_generique.php` (ancienne API non JSON)
- icones .png (utiliser l’icone svg)
