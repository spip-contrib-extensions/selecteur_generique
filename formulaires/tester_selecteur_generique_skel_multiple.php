<?php

function formulaires_tester_selecteur_generique_skel_multiple_charger_dist() {
    if (!autoriser('configurer', '_selecteur_generique')) {
        return false;
    }
    return [
        'auteurs' => '',
        'mots' => '',
        'rubriques' => '',
    ];
}

function formulaires_tester_selecteur_generique_skel_multiple_verifier_dist() {
    $keys = array_keys(formulaires_tester_selecteur_generique_skel_multiple_charger_dist());
    $values = [];
    foreach($keys as $key) {
        $values[$key] = _request($key);
    }

    return [
        'message_erreur' => '',
        'message_ok' => "Le formulaire a été verifié.\n" 
            . propre("```php\n" . var_export($values, true) . "\n```"),
    ];
}