<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

// Calcule et insere le javascript necessaire pour la page
function selecteurgenerique_inserer_javascript($flux) {
	if (defined('DESACTIVER_SELECTEUR_GENERIQUE') && constant('DESACTIVER_SELECTEUR_GENERIQUE')) {
		return $flux;
	}
	if (!test_espace_prive() && (!defined('_SELECTEUR_GENERIQUE_ACTIVER_PUBLIC') or !constant('_SELECTEUR_GENERIQUE_ACTIVER_PUBLIC'))) {
		return $flux;
	}

	$js = "\n";
	// Classe SelecteurGenerique
	$js .= "<script type='text/javascript' src='" . find_in_path('javascript/SelecteurGenerique.js') . "'></script>\n";
	$js .= "<script type='text/javascript' src='" . find_in_path('javascript/SelecteurGeneriqueLoader.js') . "'></script>\n";

	return $flux . $js;
}
