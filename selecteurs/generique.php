<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

function selecteurs_generique_dist($params = []) {

	$search = trim($params['q'] ?? '');
	$resultats = ['results' => []];

	if (!$search) {
		return json_encode($resultats);
	}

	$limite = intval($params['limite'] ?? 0) ?: 5;
	$objets = is_array($params['objets'] ?? null) ? $params['objets'] : [];
	$objets_exclus = is_array($params['objets_exclus'] ?? null) ? $params['objets_exclus'] : [];

	include_spip('base/objets');
	include_spip('inc/filtres');
	include_spip('inc/texte');

	$tables = lister_tables_objets_sql();

	if ($objets) {
		$tables = array_intersect_key($tables, array_flip(array_map('table_objet_sql', $objets)));
	}

	if ($objets_exclus) {
		$tables = array_diff_key($tables, array_flip(array_map('table_objet_sql', $objets_exclus)));
	}

	// On parcourt ensuite toutes les tables, en cherchant les meilleurs résultats par titre
	foreach ($tables as $table => $desc) {
		// Seulement si on trouve un champ titre de la table
		$champ_titre = selecteur_generique_table_titre($desc);
		if (!$champ_titre) {
			continue;
		}

		$cle_objet = id_table_objet($table);
		$objet = objet_type($table);

		// Seulement quand ça débute pareil en priorité
		$trouve = sql_allfetsel(
			$champ_titre . ', ' . $cle_objet,
			$table,
			"$champ_titre LIKE " . sql_quote("{$search}%"),
			'',
			'',
			"0,$limite"
		);
		if (!$trouve) {
			// Sinon n'importe où dans le titre
			$trouve = sql_allfetsel(
				$champ_titre . ', ' . $cle_objet,
				$table,
				"$champ_titre LIKE " . sql_quote("%{$search}%"),
				'',
				'',
				"0,$limite"
			);
		}
		if (!$trouve) {
			continue;
		}

		$group = [
			'text' => _T($desc['texte_objets']),
			'children' => [],
		];

		foreach ($trouve as $resultat) {
			$id_objet = $resultat[$cle_objet];

			$titre = appliquer_traitement_champ(
				$resultat[$champ_titre],
				'titre',
				table_objet($table),
				['objet' => $objet, 'id_objet' => $id_objet]
			);

			$group['children'][] = [
				'id' => $objet.$id_objet,
				'text' => $titre,
			];
		}

		$resultats['results'][] = $group;
	}

	return json_encode($resultats);
}


/** 
 * Si la description de table contient un titre, retourne le nom du champ
 * @param array $desc 
 * @return string
 */
function selecteur_generique_table_titre(array $desc = []) {
	// S'il y a une déclaration ET que c'est un truc super simple du genre "champ_simple AS titre"
	if (
		isset($desc['titre'])
		&& preg_match(';(?:^|,)\s*([^,\s]+)\s*as\s*titre\s*(,|$);i', $desc['titre'], $champ)
		&& !preg_match(';\W;', $champ[1])
	) {
		return trim($champ[1]);
	}
	if (isset($desc['field']['titre'])) {
		return 'titre';
	}
	if (isset($desc['field']['nom'])) {
		return 'nom';
	}
	return '';
}