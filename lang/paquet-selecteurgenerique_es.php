<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-selecteurgenerique?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'selecteurgenerique_description' => 'Reemplaza los selectores (autores, palabras, secciones) por algo más ligero y rápido, capaz de tratar tantos datos como sea posible. ',
	'selecteurgenerique_nom' => 'Selector genérico',
	'selecteurgenerique_slogan' => 'Mejora de los selectores del espacio privado'
);
