<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-selecteurgenerique?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'selecteurgenerique_description' => 'Sostituisce i selettori (autori, parole, argomenti) con qualcosa di più leggero e veloce, in grado di elaborare quanti più dati possibile.',
	'selecteurgenerique_nom' => 'Selettore generico',
	'selecteurgenerique_slogan' => 'Miglioramento dei selettori dell’area privata'
);
