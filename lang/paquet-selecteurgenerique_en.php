<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-selecteurgenerique?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'selecteurgenerique_description' => 'Replaces selectors (authors, words, sections) to something lighter and faster, can handle as much data as possible.',
	'selecteurgenerique_nom' => 'Generic selector',
	'selecteurgenerique_slogan' => 'Improving the selectors of private space'
);
