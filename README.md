# Sélecteur Générique

Le sélecteur générique offre une solution d’autocomplétion sur des champs input.

## Sélecteurs explicités

Les attribut `data-selecteur` permettent d’activer une autocompletion.
Cela définit sur quel squelette ou recherche PHP la recherche s’effectue.

### Squelette

Recherche utilisant le squelette selecteurs/auteur.html

```html
<input type="text" value="" data-selecteur="auteur" />
```

Recherche multiple utilisant le squelette selecteurs/mot.html

```html
<input type="text" value="" data-selecteur="mot" />
```


### PHP

Recherche retournant un `{objet}{id_objet}` de SPIP

```html
<input type="text" value="" 
    data-selecteur="generique" 
    data-select-php="oui" />
```

Recherche multiple (sépare par virgules)

```html
<input type="text" value="" 
    data-selecteur="generique" 
    data-select-php="oui" 
    multiple />
```

Limiter la recherche à certains objets

```html
<input type="text" value="" 
    data-selecteur="generique" 
    data-select-php="oui" 
    data-select-params='{"objets": ["articles", "rubriques"]}' />
```

Exclure de la recherche certains objets

```html
<input type="text" value="" 
    data-selecteur="generique" 
    data-select-php="oui" 
    data-select-params='{"objets_exclus": ["paquets", "plugins", "sites"]}' />
```

## Autorisations

### autoriser `autocomplete`

Le retour de l’api json se fait sur l’autorisation `autocomplete`, qui est par défaut
sur l’autorisation d’accès à l’espace privé.

L’autorisation est surchargeable notamment par sélecteur, tel que, pour un `selecteurs/demo.html`
ou `selecteurs/demo.php`, l’autorisation `autoriser('autocomplete', 'demo')` sera appelée, 
et donc la fonction `autoriser_demo_autocomplete_dist` si elle existe.

Dedans, `$opt['args']` contient les paramètres d’appel de l’api.