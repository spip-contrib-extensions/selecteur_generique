class SelecteurGenerique {

	/**
	 * Retourne l’URL standard de l’api du sélecteur générique.
	 *
	 * @param {string} selecteur Permet d’indiquer un nom de sélecteur spécifique.
	 * @returns {string}
	 */
	static api(selecteur = '') {
		const api = (spipConfig?.core?.test_espace_prive || false) ? '../selecteur.api/' : 'selecteur.api/';
		return api + selecteur;
	}

	/**
	 * Transforme des datas ancien format (jquery.ui.autocomplete) dans le format Select2
	 */
	static processData(_data) {
		if (_data.results) {
			return _data;
		}
		if (console) {
			console.info("Deprecated data format : Use select2 json structure instead of jquery.ui.autocomplete");
		}
		const data = {}
		data.results = _data.map(d => {
			return {
				"id": d.id || d.value,
				"text": d.label
			};
		});
		return data;
	}

	/**
	 * Retourne un objet avec les champs 'objet', 'id_objet' et 'source' d’un formulaire de lien.
	 * @param {Node} input
	 * @return {Object}
	 */
	static formulaire_editer_liens_params(input) {
		const form = input.closest('.formulaire_editer_liens');
		const params = {
			'objet': null,
			'id_objet': null,
			'objet_source': null,
		};
		if (form) {
			// SPIP 5
			if (form.dataset.objet || '') {
				params.objet = form.dataset.objet;
				params.id_objet = form.dataset.idObjet;
				params.objet_source = form.dataset.objetSource;
				return params;
			}
			// before SPIP 5…
			form.classList.forEach(cls => {
				// recherche de formulaire_editer_liens-auteurs-article-3-auteur
				if (cls.startsWith('formulaire_editer_liens-')) {
					const expr = cls.split('-');
					if (expr.length === 5) {
						const source = expr[1];
						params.objet = expr[2];
						params.id_objet = expr[3];
						// perfectible
						params.objet_source = (source[source.length - 1] === 's') ? source.substring(0, source.length - 1) : source;
						return params;
					}
				}
			});
		}
		return params;
	}

	/**
	 * Retourne un objet avec les champs 'objet' et 'id_*' des paramètres hidden d’un formulaire Spip.
	 * @param {Node} input
	 * @return {Object}
	 */
	static formulaire_spip_hidden_ids(input) {
		const hiddens = input.closest('form').querySelector('.form-hidden');
		const params = {};
		if (hiddens.querySelector('input[name=objet]')) {
			params['objet'] = hiddens.querySelector('input[name=objet]').value;
		}
		hiddens.querySelectorAll('input[name^=id_]').forEach(input => {
			params[input.name] = input.value;
		});
		return params;
	}

	/**
	 * Applique un autocomplete sur un input dans un formulaire de liens
	 * @param {Node} input
	 * @param {string} api URL d’API d’autocomplete
	 * @param {object} params
	 */
	static formulaire_editer_liens_autocomplete(input, api, params = {}) {
		const args = SelecteurGenerique.formulaire_editer_liens_params(input);
		SpipSelect2.on_input(input, {
			ajax: {
				url: api,
				data: (p) => { return { ...args, ...params, q: p.term }; },
				processResults: data => SelecteurGenerique.processData(data),
			},
			events: {
				'select2:selecting': e => {
					const item = e.params.args.data || {};
					// saisie de recherche
					if (item.tag) {
						input.value = item.text;
						input.parentNode.querySelector('.tout_voir,.submit').click();
						return false;
					}
					// saisie existante
					if (item.id) {
						const button = document.createElement('button');
						button.setAttribute('id', 'autocompleter');
						button.setAttribute('type', 'submit');
						button.setAttribute('name', 'ajouter_lien[' + args.objet_source + '-' + item.id + '-' + args.objet + '-' + args.id_objet + ']');
						button.setAttribute('value', '+');
						input.closest('form').append(button);
						input.setAttribute('name', ''); // supprimer la recherche
						button.click();
					}
				},
			}
		});
	}

	/**
	 * Applique un autocomplete sur un input
	 *
	 * @param {Node} input
	 * @param {string} api URL d’API d’autocomplete
	 * @param {object} params
	 */
	static on_input = function (input, api, params = {}) {
		if (input.dataset.selecteurGeneriqueOn) {
			return;
		}
		input.dataset.selecteurGeneriqueOn = 'on';
		const callback = input.dataset.selectCallback;
		const separator = input.dataset.selectSep || ',';
		const multiple = input.hasAttribute('multiple');

		let args = input.dataset.selectParams || {};
		if (typeof args === 'string') {
			try {
				args = JSON.parse(args);
			} catch (e) {
				console && console.error('Erreur dans l’analyse des paramètres supplémentaires', e);
			}
		}
		if (typeof args !== 'object') {
			args = {};
		}

		if (multiple) {
			input.dataset.separator = separator;
		}

		const options = {
			ajax: {
				url: api,
				data: (p) => { return { ...params, ...args, q: p.term }; },
				processResults: data => SelecteurGenerique.processData(data),
			}
		};

		if (callback) {
			const fn = eval(callback);
			if (typeof fn === 'function') {
				options.events = {
					'select2:select': fn
				}
			} else {
				console && console.error('Unknown data-select-callback function : ' + callback);
			}
		}
		SpipSelect2.on_input(input, options);
	};
}
