class SelecteurGeneriqueLoader {

	static formulaire_editer_liens_auteurs() {
		document.querySelectorAll('.formulaire_editer_liens .auteurs input[name=recherche]:not([autocomplete=off])')
			.forEach(input => SelecteurGenerique.formulaire_editer_liens_autocomplete(input, SelecteurGenerique.api('auteur')));
	}

	static formulaire_editer_liens_mots() {
		document.querySelectorAll('.formulaire_editer_liens .associer_mot input.text[name^=recherche\\[]:not([autocomplete=off])')
			.forEach(input => {
				const id_groupe = input.name.substring('recherche['.length, input.name.length - 1);
				SelecteurGenerique.formulaire_editer_liens_autocomplete(input, SelecteurGenerique.api('mot'), {
					"id_groupe": [id_groupe]
				})
			});
	};

	static inputs_data_selecteur() {
		document.querySelectorAll('input[data-selecteur]:not([autocomplete=off]):not([data-selecteur-generique-on])')
			.forEach(input => {
				const selecteur = input.dataset.selecteur || 'generique';
				const php = input.dataset.selectPhp || ((selecteur === 'generique') ? 'oui' : '');
				SelecteurGenerique.on_input(input, SelecteurGenerique.api(selecteur), {
					"php": php
				});
			});
	}

	static onReady(fn) {
		if (document.readyState !== 'loading') {
			fn();
		} else {
			document.addEventListener('DOMContentLoaded', fn);
		}
	}

	static load() {
		SelecteurGeneriqueLoader.formulaire_editer_liens_auteurs();
		SelecteurGeneriqueLoader.formulaire_editer_liens_mots();
		SelecteurGeneriqueLoader.inputs_data_selecteur();
	}
}

SelecteurGeneriqueLoader.onReady(SelecteurGeneriqueLoader.load);
onAjaxLoad(SelecteurGeneriqueLoader.load);
